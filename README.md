# LibreOffice 7.1.0.3 de 32 bit


LibreOffice 7.1.0.3 de 32 bit DEB compilado por [Escuelas Linux](https://escuelaslinux.sourceforge.io/). Funciona en MX Linux 19, antiX 19 (Basados en Debian 10 Buster).

Las instrucciones están en:<br />

**LibreOffice 7.1.0.3 de 32 bit DEB compilado por Escuelas Linux para Debian, basados en Ubuntu, Linux Mint y otros** 
[https://facilitarelsoftwarelibre.blogspot.com/2021/02/libreoffice-7.1.0.3-de-32-bits.html](https://facilitarelsoftwarelibre.blogspot.com/2021/02/libreoffice-7.1.0.3-de-32-bits.html) 




**Probado en:**

**- [MX Linux 19 de 32 bits](https://mxlinux.org/)**[ ](https://mxlinux.org/)

**- [antiX 19 de 32 bits](https://antixlinux.com/)** (Pero no aparecen los iconos entre las aplicaciones, pero se puede lanzar desde el lanzador, ver solución: [aquí](https://facilitarelsoftwarelibre.blogspot.com/2019/09/menu-de-apps-antix-no-recarga-iconos.html))

**- [Linux Mint Debbie 4 de 32 bits](https://www.linuxmint.com/download_lmde.php)**

<div style="text-align: left;">**Nota:** Viene pre-instalado en [Escuelas Linux](https://escuelaslinux.sourceforge.io/)  
</div>

### **Purgar LibreOffice**

Antes de instalar LibreOffice 7.1.0.3 lo primero es remover el LibreOffice que venía preinstalado en su distribución, o, si ya lo desisntaló asegurese que NO quede algún paquete residual, en ambos casos ponga en la terminal:  

<pre class="linux-code" style="background-attachment: scroll; background-clip: border-box; background-color: #e7e8e9; background-origin: padding-box; background-position: 0px 0px; background-repeat: no-repeat; background-size: auto; background: url(&quot;https://lh3.googleusercontent.com/-E2WZ-k5ArbU/VnnAeX-_qmI/AAAAAAAABDU/i1aaUUYLZh8/s540-Ic42/lincodewachin.gif&quot;) 0px 0px no-repeat scroll rgb(231, 232, 233); border-color: rgb(214, 73, 55); border-style: solid; border-width: 1px 1px 1px 20px; font-family: &quot;UbuntuBeta Mono&quot;, &quot;Ubuntu Mono&quot;, &quot;Courier New&quot;, Courier, monospace; line-height: 22.4px; margin: 10px; max-height: 500px; min-height: 16px; overflow: auto; padding: 28px 10px 10px; z-index: 10000;">sudo apt-get remove --purge libreoffice*</pre>

aparece el mensaje que pregunta si y poner "s" que sí:  
 [![](https://3.bp.blogspot.com/-hUJODKmenl0/XpXAhnJNo9I/AAAAAAAAbIE/se1lk6hX-ogv0QAMiX60igFX91N0Z3qjACK4BGAYYCw/s1600/20200414-084010.png)](https://3.bp.blogspot.com/-hUJODKmenl0/XpXAhnJNo9I/AAAAAAAAbIE/se1lk6hX-ogv0QAMiX60igFX91N0Z3qjACK4BGAYYCw/s1600/20200414-084010.png)  

escriba la:  

s  

y de Enter.

<span></span>

## <span style="color: blue;">**INSTALACIÓN**</span>

Éntre en la siguiente dirección web (Los paquetes DEB de 32 bit han sido compilados por [Escuelas Linux)](https://escuelaslinux.sourceforge.io/), de clic:  

[https://gitlab.com/wachin/libreoffice-7.1.0.3-de-32-bits]( https://gitlab.com/wachin/libreoffice-7.1.0.3-de-32-bits)  

y de clic así como le muestro en la siguiente imagen:  

[![](https://lh3.googleusercontent.com/-iufYeEzctEY/YDaBOASwzuI/AAAAAAAAgOI/Ch8NNvNXCn0-hSt_o7ZfmKn9W7q2NJLVACLcBGAsYHQ/s16000/20210224-113621.png)](https://lh3.googleusercontent.com/-iufYeEzctEY/YDaBOASwzuI/AAAAAAAAgOI/Ch8NNvNXCn0-hSt_o7ZfmKn9W7q2NJLVACLcBGAsYHQ/20210224-113621.png)

dele clic en "zip"  

Si no tiene configurado el navegador web para que le pregunte dónde guardar se guardará automáticamente en Descargas  

Ahora, cuando se haya descargado, vaya hasta allí en el administrador de archivos que usted use y extraiga el contenido, clic derecho Extraer aquí, entre en la carpeta descomprimida, y entre en la carpeta DEB:  

[![](https://1.bp.blogspot.com/-GOh4aGm7ZZE/XuOmrZKejbI/AAAAAAAAcUg/blGgt8EK8cckHWw1N17pedxFwVt37YkSQCK4BGAYYCw/s1600/20200612-104435.png)](https://1.bp.blogspot.com/-GOh4aGm7ZZE/XuOmrZKejbI/AAAAAAAAcUg/blGgt8EK8cckHWw1N17pedxFwVt37YkSQCK4BGAYYCw/s1600/20200612-104435.png)  

Ahora elija el método que sea el mejor para usted para abrir una terminal allí, sino sabe puede seguir [este](https://facilitarelsoftwarelibre.blogspot.com/2017/11/como-llegar-un-directorio-especifico.html) tutorial o [este](https://facilitarelsoftwarelibre.blogspot.com/2020/02/abrir-terminal-aqui-con-administradores.html) otro

una vez allí dentro en la terminal hay que poner:  

<pre class="linux-code" data-pf_rect_height="133.59999084472656" data-pf_rect_width="805" data-pf_style_display="block" data-pf_style_visibility="visible" style="background-attachment: scroll; background-clip: border-box; background-color: #e7e8e9; background-origin: padding-box; background-position: 0px 0px; background-repeat: no-repeat; background-size: auto; background: url(&quot;https://lh3.googleusercontent.com/-E2WZ-k5ArbU/VnnAeX-_qmI/AAAAAAAABDU/i1aaUUYLZh8/s540-Ic42/lincodewachin.gif&quot;) 0px 0px no-repeat scroll rgb(231, 232, 233); border-color: rgb(214, 73, 55); border-style: solid; border-width: 1px 1px 1px 20px; font-family: &quot;UbuntuBeta Mono&quot;, &quot;Ubuntu Mono&quot;, &quot;Courier New&quot;, Courier, monospace; line-height: 22.4px; margin: 10px; max-height: 500px; min-height: 16px; overflow: auto; padding: 28px 10px 10px; z-index: 10000;"><span data-pf_rect_height="89.19999694824219" data-pf_rect_width="138.60000610351562" data-pf_style_display="inline" data-pf_style_visibility="visible" style="font-size: medium;">sudo dpkg -i *.deb</span></pre>

la siguiente imagen es en Nemo al dar clic derecho (Nemo necesita tener instalado gnome-terminal):  

[![](https://1.bp.blogspot.com/-MW2XrPX1zCM/YDau-Ont0MI/AAAAAAAAgOU/H7XLlWfu1bs68f6iidSM6ihppvyx42JigCLcBGAsYHQ/s16000/20210224-145302.png)](https://1.bp.blogspot.com/-MW2XrPX1zCM/YDau-Ont0MI/AAAAAAAAgOU/H7XLlWfu1bs68f6iidSM6ihppvyx42JigCLcBGAsYHQ/s711/20210224-145302.png)

poner el comando:

[![](https://1.bp.blogspot.com/-b33EpkZH3wA/YDawmdJpGdI/AAAAAAAAgOg/Yz__Gp897q4iib8pCxmsrIUHj3yF3vqewCLcBGAsYHQ/s16000/20210224-150001.png)](https://1.bp.blogspot.com/-b33EpkZH3wA/YDawmdJpGdI/AAAAAAAAgOg/Yz__Gp897q4iib8pCxmsrIUHj3yF3vqewCLcBGAsYHQ/s739/20210224-150001.png)

ponga su contraseña y esperar a que se instale.

** Nota:** Es posible que si tenía instalado a OpenLP sea desinstalado, si fuera así debe instalarlo después de haber instalado este.  

### **Posible problema con la visualización de los iconos en la barra de tareas**  

Podría pasar que algunos temas de iconos no muestran el icono de LibreOffice 7.1 en la barra de tareas. Por eso en el lugar de su sistema operativo Linux donde esté la personalización de apariencias y donde pueda cambiar los temas de los iconos vea que puede haber temas de iconos en los que no funcione bien con esta versión de LibreOffice (Uno de los temas de iconos con que siempre funciona es Brisa):  

[![](https://3.bp.blogspot.com/-JMY1DKEC1Pw/XpuIPE1JefI/AAAAAAAAbVE/3yb1pO2JT3w_NThh3k80sWxB7GihwOBrgCK4BGAYYCw/s1600/20200418-180722.png)](https://3.bp.blogspot.com/-JMY1DKEC1Pw/XpuIPE1JefI/AAAAAAAAbVE/3yb1pO2JT3w_NThh3k80sWxB7GihwOBrgCK4BGAYYCw/s1600/20200418-180722.png)  

Bueno les digo que en cada sistema operativo es diferente el lugar donde están las opciones de personalización, pero como sea ustedes deben de buscar.  

Si desean instalar el tema Brisa pongan en la terminal  

<pre class="linux-code" data-pf_rect_height="133.59999084472656" data-pf_rect_width="805" data-pf_style_display="block" data-pf_style_visibility="visible" style="background-attachment: scroll; background-clip: border-box; background-color: #e7e8e9; background-origin: padding-box; background-position: 0px 0px; background-repeat: no-repeat; background-size: auto; background: url(&quot;https://lh3.googleusercontent.com/-E2WZ-k5ArbU/VnnAeX-_qmI/AAAAAAAABDU/i1aaUUYLZh8/s540-Ic42/lincodewachin.gif&quot;) 0px 0px no-repeat scroll rgb(231, 232, 233); border-color: rgb(214, 73, 55); border-style: solid; border-width: 1px 1px 1px 20px; font-family: &quot;UbuntuBeta Mono&quot;, &quot;Ubuntu Mono&quot;, &quot;Courier New&quot;, Courier, monospace; line-height: 22.4px; margin: 10px; max-height: 500px; min-height: 16px; overflow: auto; padding: 28px 10px 10px; z-index: 10000;"><span data-pf_rect_height="89.19999694824219" data-pf_rect_width="138.60000610351562" data-pf_style_display="inline" data-pf_style_visibility="visible" style="font-size: medium;">sudo apt install breeze-icon-theme</span></pre>

luego de instalado deben de elegirlo.  

**<span style="color: red;">Notas de esta versión</span>**

**LibreOffice 7.1 llega con división de ediciones, características experimentales y mas | Desde Linux**  
[https://blog.desdelinux.net/libreoffice-7-1-llega-con-division-de-ediciones-caracteristicas-experimentales-y-mas/](https://blog.desdelinux.net/libreoffice-7-1-llega-con-division-de-ediciones-caracteristicas-experimentales-y-mas/)

**LibreOffice 7.1 Community: la nueva versión del paquete de ofimática open source cambia de nombre para el público general**  
[https://www.genbeta.com/ofimatica/libreoffice-7-1-community-nueva-version-paquete-ofimatica-open-source-cambia-nombre-para-publico-general](https://www.genbeta.com/ofimatica/libreoffice-7-1-community-nueva-version-paquete-ofimatica-open-source-cambia-nombre-para-publico-general)

**LibreOffice 7.1, novedades y descarga de la primera beta**  
[https://www.softzone.es/noticias/open-source/novedades-libreoffice-7-1-beta-1/](https://www.softzone.es/noticias/open-source/novedades-libreoffice-7-1-beta-1/)

**Disponible LibreOffice 7.1 Community con mejor soporte de DOCX y Unicode » MuyLinux**  
[https://www.muylinux.com/2021/02/03/libreoffice-7-1-community/](https://www.muylinux.com/2021/02/03/libreoffice-7-1-community/)

Dios les bendiga



